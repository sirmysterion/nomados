client {
    enabled = true
    network_interface = "eth0"
    node_class = ""
    no_host_uuid = true

    max_kill_timeout = "30s"

    network_speed = 0
    cpu_total_compute = 0

    // gc_interval = "1m"
    // gc_disk_usage_threshold = 80
    // gc_inode_usage_threshold = 70
    // g_parallel_destroys = 2

    // reserved {
    //     cpu = 0
    //     memory = 0
    //     disk = 0
    // }

    // cni_path        = "/opt/cni/bin"
    // cni_config_dir  = "/opt/cni/config"

    // host_volume "nomad_data" {
    //     path      = "/mnt/nomad_data"
    //     read_only = false
    // }

    options {
        "docker.volumes.enabled" = "true"
    }
}



plugin "raw_exec" {
  config {
    enabled = true
  }
}

plugin "docker" {
  config {
    allow_privileged = true
    volumes {
      enabled      = true
    }
  }
}