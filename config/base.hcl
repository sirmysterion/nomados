datacenter = "dc1"

enable_debug = false
disable_update_check = false


bind_addr = "0.0.0.0"
// bind_addr = "::"
// advertise {
//     http = "[]:4646"
//     rpc  = "[]:4647"
//     serf = "[]:4648"
// }
// ports {
//     http = 4646
//     rpc = 4647
//     serf = 4648
// }

// consul {
//     # The address to the Consul agent.
//     address = "localhost:8500"
//     token = ""
//     # The service name to register the server and client with Consul.
//     server_service_name = "nomad-servers"
//     client_service_name = "nomad-clients"
//     tags = {}

//     # Enables automatically registering the services.
//     auto_advertise = true

//     # Enabling the server and client to bootstrap using Consul.
//     server_auto_join = true
//     client_auto_join = true
// }

data_dir = "/var/lib/nomad/data"

log_level = "DEBUG"
// enable_syslog = true

leave_on_terminate = true
leave_on_interrupt = false
